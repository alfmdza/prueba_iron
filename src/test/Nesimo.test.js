describe('sucesiónDeFibonacci', () => {

    const sucesiónDeFibonacci = (numero) => {

        if (numero == 0 || numero == 1) {
            return numero;
        } else {

            return sucesiónDeFibonacci(numero - 1) + sucesiónDeFibonacci(numero - 2);

        }
    }

    test('1', () => {
        expect(sucesiónDeFibonacci(1)).toBe(1);
    })

    test('2', () => {
        expect(sucesiónDeFibonacci(2)).toBe(1);
    })

    test('3', () => {
        expect(sucesiónDeFibonacci(3)).toBe(2);
    })
    test('4', () => {
        expect(sucesiónDeFibonacci(4)).toBe(3);
    })
    test('5', () => {
        expect(sucesiónDeFibonacci(5)).toBe(5);
    })
    test('6', () => {
        expect(sucesiónDeFibonacci(6)).toBe(8);
    })
    test('7', () => {
        expect(sucesiónDeFibonacci(7)).toBe(13);
    })
    test('8', () => {
        expect(sucesiónDeFibonacci(8)).toBe(21);
    })
    test('9', () => {
        expect(sucesiónDeFibonacci(9)).toBe(34);
    })
    test('10', () => {
        expect(sucesiónDeFibonacci(10)).toBe(55);
    })
});