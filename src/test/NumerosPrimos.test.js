describe('esPrimo', () => {

    const esPrimo = (numero) => {

        if (numero == 0 || numero == 1 || numero == 4) {
          return false;
        } else {
          for (let x = 2; x < numero / 2; x++) {
            if (numero % x == 0){
              return false;
            } 
          }
          return true;
        }
      }

    test('1', () => {
        expect(esPrimo(1)).toBe(false);
    })

    test('2', () => {
        expect(esPrimo(2)).toBe(true);
    })

    test('3', () => {
        expect(esPrimo(3)).toBe(true);
    })
    test('4', () => {
        expect(esPrimo(4)).toBe(false);
    })
    test('5', () => {
        expect(esPrimo(5)).toBe(true);
    })
    test('6', () => {
        expect(esPrimo(6)).toBe(false);
    })
    test('7', () => {
        expect(esPrimo(7)).toBe(true);
    })
    test('8', () => {
        expect(esPrimo(8)).toBe(false);
    })
    test('9', () => {
        expect(esPrimo(9)).toBe(false);
    })
    test('10', () => {
        expect(esPrimo(10)).toBe(false);
    })
});