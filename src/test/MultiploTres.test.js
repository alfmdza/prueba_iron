describe('multiploTres', () => {

    const multiploTres = (numero) => {

        let a = 1;
        let arrayMultiplosTres = [];
        while (a <= numero) {
            if (a % 3 == 0) {
                arrayMultiplosTres.push(a);

            }
            a = a + 1;

        }
        return arrayMultiplosTres;
    }

    test('1', () => {
        expect(multiploTres(1)).toEqual([]);
    })

    test('2', () => {
        expect(multiploTres(2)).toEqual([]);
    })

    test('3', () => {
        expect(multiploTres(3)).toEqual([3]);
    })
    test('4', () => {
        expect(multiploTres(4)).toEqual([3]);
    })
    test('5', () => {
        expect(multiploTres(5)).toEqual([3]);
    })
    test('6', () => {
        expect(multiploTres(6)).toEqual([3, 6]);
    })
    test('7', () => {
        expect(multiploTres(7)).toEqual([3, 6]);
    })
    test('8', () => {
        expect(multiploTres(8)).toEqual([3,6]);
    })
    test('9', () => {
        expect(multiploTres(9)).toEqual([3, 6, 9]);
    })
    test('10', () => {
        expect(multiploTres(10)).toEqual([3, 6, 9]);
    })
});