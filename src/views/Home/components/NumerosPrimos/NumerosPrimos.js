import React, { useState } from 'react';
import NavbarB from '../../../../components/NavbarB';
import { Container, Form, Button } from 'react-bootstrap';
import Resultado from './Resultado';

const NumerosPrimos = () => {
  const [numero, setNumero] = useState('')

  const [resultado, setResultado] = useState()

  const handleNumeroChange = event => {
    setNumero(event.target.value)
  };

  const esPrimo = (numero) => {

    if (numero == 0 || numero == 1 || numero == 4) {
      return false;
    } else {
      for (let x = 2; x < numero / 2; x++) {
        if (numero % x == 0){
          return false;
        } 
      }
      return true;
    }
  }

  const onFormSubmit = (e) => {
    e.preventDefault();
    setResultado({ numero: numero, esPrimo: esPrimo(numero) });
  }

  return (
    <div>
      <NavbarB />
      <Container>
        <h1>NumerosPrimos</h1>
        <Form onSubmit={onFormSubmit}>
          <Form.Group className="mb-3">
            <Form.Label>Número</Form.Label>
            <Form.Control type="number" min="1" placeholder="Ingrese número" name="number" onChange={handleNumeroChange}
              value={numero}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
          {
            (resultado) ? <Resultado resultadoSend={resultado} /> : null
          }

        </Form>
      </Container>


    </div>
  )


}

export default NumerosPrimos;