import React from 'react';

const Resultado = ({ resultadoSend }) => {

    return (
        <h2>Resultado: {resultadoSend.numero} {(resultadoSend.esPrimo) ? "Sí es primo" : "No es primo"}</h2>
    )
}

export default Resultado;