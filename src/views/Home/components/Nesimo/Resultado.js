import React from 'react';

const Resultado = ({ resultadoSend }) => {

    return (
        <h2>Resultado:El n-esimo termino de {resultadoSend.numero} es: {resultadoSend.sucesiónDeFibonacci}</h2>
    )
}

export default Resultado;