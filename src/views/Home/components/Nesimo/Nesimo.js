import React, { useState } from 'react';
import NavbarB from '../../../../components/NavbarB';
import { Container, Form, Button } from 'react-bootstrap';
import Resultado from './Resultado';

const Nesimo = () => {

  const [numero, setNumero] = useState('')

  const [resultado, setResultado] = useState();

  const handleNumeroChange = event => {
    setNumero(event.target.value)
  };

  const sucesiónDeFibonacci = (numero) => {

    if (numero == 0 || numero == 1) {
      return numero;
    }else{

      return sucesiónDeFibonacci(numero-1) + sucesiónDeFibonacci(numero-2);

    }
  }

  const onFormSubmit = (e) => {
    e.preventDefault();
    setResultado({ numero: numero, sucesiónDeFibonacci: sucesiónDeFibonacci(numero) });
  }
  return (

    <div>
      <NavbarB />

      <Container>
        <h1>N-esimo de cada serie</h1>
        <Form onSubmit={onFormSubmit}>
          <Form.Group className="mb-3">
            <Form.Label>Número</Form.Label>
            <Form.Control type="number" min="1" placeholder="Ingrese número" name="number" onChange={handleNumeroChange}
              value={numero} />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
          {
            (resultado) ? <Resultado resultadoSend={resultado} /> : null
          }
        </Form>
      </Container>
    </div>
  )


}

export default Nesimo;