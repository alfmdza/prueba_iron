import React, { useState } from 'react';
import NavbarB from '../../../../components/NavbarB';
import { Container, Form, Button } from 'react-bootstrap';
import Resultado from './Resultado';

const MultiploTres = () => {

  const [numero, setNumero] = useState('')

  const [resultado, setResultado] = useState();

  const handleNumeroChange = event => {
    setNumero(event.target.value)
  };

  const multiploTres = (numero) => {

    let a = 1;
    let arrayMultiplosTres=[];
    while (a <= numero) {
      if (a % 3 == 0) {
        arrayMultiplosTres.push(a);

      }
      a = a + 1;

    }
    return arrayMultiplosTres;
  }


  const onFormSubmit = (e) => {
    e.preventDefault();
    setResultado({ numero: numero, multiploTres: multiploTres(numero) });
  }
  return (

    <div>
      <NavbarB />
      <Container>
        <h1>Multiplos</h1>
        <Form onSubmit={onFormSubmit}>
          <Form.Group className="mb-3">
            <Form.Label>Número</Form.Label>
            <Form.Control type="number" min="3" placeholder="Ingrese número" name="number" onChange={handleNumeroChange}
              value={numero} />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
          {
            (resultado) ? <Resultado resultadoSend={resultado} /> : null
          }
        </Form>

      </Container>
    </div>
  )


}

export default MultiploTres;