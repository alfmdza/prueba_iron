import React from 'react';

const Resultado = ({ resultadoSend }) => {

    return (
        <div>
            <h2>Resultado: Los múltiplos de tres hasta el número {resultadoSend.numero} son:</h2>
            {resultadoSend.multiploTres.map((name, index) => (
                <li key={index}>
                    {name}
                </li>
            ))}
        </div>


    )
}

export default Resultado;