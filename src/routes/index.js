import React from "react";
import {
    createBrowserRouter,
} from "react-router-dom";
import Home from "../views/Home";
import Nesimo from "../views/Home/components/Nesimo/Nesimo";
import NumerosPrimos from "../views/Home/components/NumerosPrimos/NumerosPrimos";
import MultiploTres from "../views/Home/components/MultiploTres/MultiploTres";



const RoutesComponent = createBrowserRouter([
    {
        path: "/",
        element: <Home />,
    },
    {
        path: "/nesimo",
        element: <Nesimo />,
    },
    {
        path: "/multiplode3",
        element: <MultiploTres />,
    },
    {
        path: "/numerosprimos",
        element: <NumerosPrimos />,
    },
]);

export default RoutesComponent;