import { Container, Nav, Navbar } from "react-bootstrap";
import React from 'react';

const NavbarB = () => {
    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="/">Home</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/nesimo">N-esimo</Nav.Link>
                        <Nav.Link href="/multiplode3">Multiplo de 3</Nav.Link>
                        <Nav.Link href="/numerosprimos">Números primos</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}
export default NavbarB;